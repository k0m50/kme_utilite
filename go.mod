module kme

go 1.21.0

require (
	github.com/BurntSushi/toml v1.3.2
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
)

require (
	golang.org/x/sys v0.12.0 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
)
