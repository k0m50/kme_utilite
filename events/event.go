package events

import (
	"fmt"
	"kme/iec60870"
	"kme/iec60870/iec101"
	"kme/iec60870/iec104"
	"os"
	"time"
)

func StartServer(iec104context *iec104.APCI) {
	readBuffer := make([]byte, 1000, 1000)
	iec104context.SendUframe(iec104.StartAct)
	//TODO: send expected frame
	time.Sleep(1 * time.Second)
	iec104context.ReadData(readBuffer)
	iec104context.SendRawFrame(iec101.Init)
	time.Sleep(1 * time.Second)
	iec104context.ReadData(readBuffer)
	time.Sleep(4 * time.Second)
	//readBuffer = nil
	for len := 0; len == 0; {
		len, _ = iec104context.ReadData(readBuffer)
	}
	readBuffer2 := make([]byte, 1000, 1000)
	for {
		var command string
		fmt.Println("Write command")
		fmt.Fscan(os.Stdin, &command)

		switch command {
		case getFWversion:
			iec104context.SendIframe(iec101.ReadObject(iec60870.FWaddress))
		case getDump:
			iec104context.SendIframe(iec101.ReadObject(iec60870.DumpAdress))
		case HF:
			iec104context.SendRawFrame(iec101.Reset)
		default:
			fmt.Printf("%s - uncorrect command\n", command)
			continue
		}
		//time.Sleep(1 * time.Second
		for lenRx := 0; lenRx == 0; {
			lenRx, _ = iec104context.ReadData(readBuffer2)
		}
		iec104context.Handle(readBuffer2)
	}
}
