package iec104

import (
	"errors"
	"fmt"
	"kme/iec60870/iec101"
	"kme/iec60870/transport"
)

type APCI struct {
	typeASDU  uint8
	countSend uint16
	k         uint8
	w         uint8
	transport transport.Transport
}

type Settings struct {
	K uint8
	W uint8
}

const (
	typeU = iota
	typeI
	typeS
)

var StartAct = []byte{0x68, 0x04, 0x07, 0x00, 0x00, 0x00}

const StartByte = 0x68

func New(setting Settings, tr transport.Transport) *APCI {

	a := APCI{
		countSend: 0,
		transport: tr,
		k:         setting.K,
		w:         setting.W,
	}
	return &a
}

func (a *APCI) Handle(byte []byte) error {
	if err := a.deserializeAsdu(byte); err != nil {
		return errors.New("no defirialize")
	}

	switch a.typeASDU {
	case typeU:
		fmt.Println("recive u frame")
		handleUFrame(byte)
	case typeI:
		fmt.Println("recive I frame")
		handleIFrame(byte)
	case typeS:
		fmt.Println("recive S frame")
		handleSFrame(byte)
	}
	return nil
}

func (a *APCI) deserializeAsdu(byte []byte) error {

	a.checkAsduType(byte[2])
	if byte[0] != StartByte {
		return errors.New("no start byte")
	}
	return nil
}

func (a *APCI) checkAsduType(byte uint8) {

	if byte&0x03 == 0x03 {
		a.typeASDU = typeU
	} else if byte&0x03 == 0x01 {
		a.typeASDU = typeS
	} else {
		a.typeASDU = typeI
	}
}

func handleUFrame(byte []byte) {
	fmt.Println("U frame process")
}

func handleSFrame(byte []byte) {
	fmt.Println("I frame process")
}

func handleIFrame(byte []byte) error {
	d := iec101.New(byte)
	d.Handler(byte)
	return nil
}

func (a *APCI) SendIframe(bytes []byte) error {

	var apciData = make([]byte, 6, 20)
	a.countSend++
	apciData[0] = 0x68
	apciData[1] = uint8(len(bytes) + 4)
	apciData[2] = 0
	apciData[3] = 0
	apciData[4] = 0
	apciData[5] = 0
	pdu := append(apciData, bytes...)

	if err := a.transport.Send(pdu); err != nil {
		return errors.New("gg send data")
	}
	return nil
}

func (a *APCI) SendSTARTAct() {
	a.transport.Send(StartAct)
}

func (a *APCI) SendUframe(uframe []byte) {
	a.transport.Send(uframe)
}

func (a *APCI) SendRawFrame(rawByte []byte) {
	a.transport.Send(rawByte)
}

func (a *APCI) SendINITct() {
	a.transport.Send(iec101.Init)
}

func (a *APCI) ReadData(bytes []byte) (int, error) {
	return a.transport.Recieve(bytes)
}
