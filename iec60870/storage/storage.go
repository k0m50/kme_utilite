package storage

import (
	"gopkg.in/ini.v1"
)

type COMSection struct {
	COM  string
	Baud int
}
type TCPSection struct {
	IP   string
	Port int
}

type Config struct {
	COM COMSection `ini:"COM Section"`
	TCP TCPSection `ini:"TCP Section"`
}

func Configuration(file string) (*Config, error) {
	cfg, err := ini.Load(file)
	if err != nil {
		return nil, err
	}
	var c Config
	cfg.MapTo(&c)
	return &c, nil
}
