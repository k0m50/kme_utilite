package transport

import (
	"net"
)

type Transport interface {
	Send(byte []byte) error
	Recieve(byte []byte) (int, error)
}

type tcpTransport struct {
	transport *net.TCPConn
}

func NewTCP(network string, laddr, raddr *net.TCPAddr) (*tcpTransport, error) {
	connTCP, err := net.DialTCP(network, laddr, raddr)
	t := tcpTransport{
		transport: nil,
	}
	if err != nil {
		return nil, err
	}
	t.transport = connTCP
	return &t, nil
}

func (t *tcpTransport) Send(byte []byte) error {
	_, err := t.transport.Write(byte)
	return err
}

func (t *tcpTransport) Recieve(byte []byte) (int, error) {
	return t.transport.Read(byte)
}
