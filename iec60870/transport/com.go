package transport

import (
	"fmt"
	"github.com/tarm/serial"
)

type comTransport struct {
	transport *serial.Port
}

func NewCOM(config *serial.Config) (*comTransport, error) {

	s, err := serial.OpenPort(config)
	if err != nil {
		fmt.Println("can't open com")
		return nil, err
	}
	c := comTransport{
		transport: s,
	}
	return &c, nil
}

func (t *comTransport) Send(byte []byte) error {
	_, err := t.transport.Write(byte)
	return err
}

func (t *comTransport) Recieve(byte []byte) (int, error) {
	return t.transport.Read(byte)
}
