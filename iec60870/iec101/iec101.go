package iec101

import (
	"encoding/binary"
	"fmt"
	"math"
)

var Init = []byte{0x68, 0x0e, 0x14, 0x00, 0x00, 0x00, 0x64, 0x01, 0x06, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x14}
var Reset = []byte{0x68, 0x0e, 0x04, 0x00, 0x22, 0x00, 0x2d, 0x01, 0x06, 0x00, 0xff, 0xff, 0x7e, 0x00, 0x00, 0x01}

//IEC60870_5_TypeID_t             TypeID;
//IEC101_VSQ_t                    QStruct;
//IEC101_CauseOfTransmission_t    Cause;
//uint8_t                         CauseAO;
//IEC101_address_oa_t             addrOA;
//IEC101_address_ioa_t            addrIOA;
//uint8_t                         size;
//uint8_t                         size_byte;
//uint8_t*                        time56;

type dui struct {
	typeID  uint8
	qStruct uint8
	cause   uint8
	causeAO uint8
	//addressOA
}

const (
	M_ME_TF_1     = 36
	SERIAL_NUMBER = 39
	FW            = 143
	DUMP          = 144
	RESET_COMMAND = 193
)

func New(byte []byte) *dui {
	var d dui
	d.deserializeApci(byte)
	return &d
}

func (d *dui) Handler(byte []byte) error {
	d.deserializeApci(byte)

	switch d.typeID {
	case M_ME_TF_1:
		d.handlerFloatPointValue(byte)
	case FW:
		d.handlerFW(byte)
	case SERIAL_NUMBER:
		//handlerSerialNumber(byte)
	case DUMP:
		d.handlerDump(byte[15:])
	}
	return nil
	//if err != nil {
	//	return errors.New("no deserialize iec101")
	//}
}

func (d *dui) deserializeApci(byte []byte) {

	d.typeID = byte[6]
	d.qStruct = byte[7]

}
func (d *dui) handlerFloatPointValue(byte []byte) {
	fv := Float64frombytes(byte[15:])
	fmt.Println("Recieve data M_ME_TF_1")
	fmt.Printf("value = %f", fv)
}

func (d *dui) handlerFW(byte []byte) {
	//fv := Float64frombytes(byte[15:])
	fw := string(byte[15:20])
	fmt.Println("Recieve data Frimvare Version")
	fmt.Printf("version = %s \n\r", fw)
}

// uint32_t   Event;
// struct{
// uint32_t  r0;
// uint32_t  r1;
// uint32_t  r2;
// uint32_t  r3;
// uint32_t r12;
// uint32_t  lr;  /* Link register. */
// uint32_t  pc;  /* Program counter. */
// uint32_t psr;  /* Program status register. */
// uint32_t  sp;  /* Stack pointer register. */
// uint32_t exc_ret;
// };
// uint32_t ext;
// struct{
// uint32_t bfar;
// uint32_t mmfar;
// uint32_t cfsr;
// uint32_t hfsr;
// uint32_t dfsr;
// uint32_t afsr;
// };

func (d *dui) handlerDump(byte []byte) {
	i := 0
	fmt.Printf("Event \t-\t 0x%X\r\n", BinaryToUint32(byte, &i))
	fmt.Printf("r0 \t-\t 0x%X\r\n", BinaryToUint32(byte, &i))
	fmt.Printf("r1 \t-\t 0x%X\r\n", BinaryToUint32(byte, &i))
	fmt.Printf("r2 \t-\t 0x%X\r\n", BinaryToUint32(byte, &i))
	fmt.Printf("r3 \t-\t 0x%X\r\n", BinaryToUint32(byte, &i))
	fmt.Printf("r12 \t-\t 0x%X\r\n", BinaryToUint32(byte, &i))
	fmt.Printf("lr \t-\t 0x%X\r\n", BinaryToUint32(byte, &i))
	fmt.Printf("pc \t-\t 0x%X\r\n", BinaryToUint32(byte, &i))
	fmt.Printf("psr \t-\t 0x%X\r\n", BinaryToUint32(byte, &i))
	fmt.Printf("sp \t-\t 0x%X\r\n", BinaryToUint32(byte, &i))
	fmt.Printf("exc_ret -\t 0x%X\r\n", BinaryToUint32(byte, &i))

	fmt.Printf("ext \t-\t 0x%X\r\n", BinaryToUint32(byte, &i))

	fmt.Printf("bfar \t-\t 0x%X\r\n", BinaryToUint32(byte, &i))
	fmt.Printf("mmfar \t-\t 0x%X\r\n", BinaryToUint32(byte, &i))
	fmt.Printf("cfsr \t-\t 0x%X\r\n", BinaryToUint32(byte, &i))
	fmt.Printf("hfsr \t-\t 0x%X\r\n", BinaryToUint32(byte, &i))
	fmt.Printf("dfsr \t-\t 0x%X\r\n", BinaryToUint32(byte, &i))
	fmt.Printf("afsr \t-\t 0x%X\r\n", BinaryToUint32(byte, &i))

}

func BinaryToUint32(byte []byte, pos *int) uint32 {
	uint32_ := binary.LittleEndian.Uint32(byte[*pos:])
	*pos += 4
	return uint32_
}

func Float64frombytes(bytes []byte) float32 {
	bits := binary.LittleEndian.Uint32(bytes)
	float := math.Float32frombits(bits)
	return float
}

func ReadObject(addr uint8) []byte {
	var asduData = make([]byte, 9, 9)

	asduData[0] = 102
	asduData[1] = 1
	asduData[2] = 6
	asduData[3] = 0
	asduData[4] = 1
	asduData[5] = 0
	asduData[6] = addr
	asduData[7] = 0
	asduData[8] = 0

	return asduData
}
