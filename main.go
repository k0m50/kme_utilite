package main

import (
	"fmt"
	"github.com/tarm/serial"
	"kme/events"
	"kme/iec60870/iec104"
	"kme/iec60870/storage"
	"kme/iec60870/transport"
	"net"
	"os"
)

func main() {

	var iec104context *iec104.APCI

	setting := iec104.Settings{W: 10, K: 5}

	cfg, err := storage.Configuration("config.ini")
	if err != nil {
		fmt.Println("can't read cfg file")
	}

	fmt.Println("Select transport layer:")
	fmt.Println("COM: 1")
	fmt.Println("TCP: 2")
	fmt.Println("Write number layer ")
	var layer string
	fmt.Fscan(os.Stdin, &layer)

selectLayer:
	if layer == "1" {
		fmt.Println("Layer COM")
		fmt.Printf("COM - %s\n\r", cfg.COM.COM)
		fmt.Printf("Baud - %d\n\n", cfg.COM.Baud)

		comConfig := serial.Config{
			Baud: cfg.COM.Baud,
			Name: cfg.COM.COM,
		}
		comTransport, err := transport.NewCOM(&comConfig)
		if err != nil {
			fmt.Println("can't creat COM connection")
		}

		iec104context = iec104.New(setting, comTransport)
	} else if layer == "2" {
		fmt.Println("Layer TCP")
		fmt.Printf("IP - %s\n\r", cfg.TCP.IP)
		fmt.Printf("Port - %d\n\n", cfg.TCP.Port)

		tcpAddr := net.TCPAddr{
			IP:   net.ParseIP(cfg.TCP.IP), //cfg.TCP.IP,
			Port: cfg.TCP.Port,
			Zone: "",
		}
		tcpTransport, err := transport.NewTCP("tcp4", nil, &tcpAddr)
		if err != nil {
			fmt.Println("can't creat TCP connection")
		}

		iec104context = iec104.New(setting, tcpTransport)
	} else {
		fmt.Println("uncorrected layer")
		goto selectLayer
	}

	events.StartServer(iec104context)

}
